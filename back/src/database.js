let redis = require('redis')
let client = redis.createClient({
  //port      : process.env.REDIS_PORT,               // replace with your port
  host      : process.env.REDIS_HOST,        // replace with your hostanme or IP address
  // host      : '192.168.99.100',// replace with your hostanme or IP address
  // password  : 'password'       // replace with your password
})

client.on('connect', function() {
  console.log('Redis DB connected')
})

client.on("error", function (err) {
  console.log("Redis Error: " + err)
})

module.exports = client
