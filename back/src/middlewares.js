exports.validatePageParam = (req, res, next) => {
  const page = req.query.page || 1
  if (isNaN(page) || page <= 0 ) {
    req.query.page = 1
  }
  next()
}

exports.validateUsernameAndPassword = (req, res, next) => {
  const username = req.body.username
  const password = req.body.password
  let errors = []

  if (typeof username == 'undefined') {
    errors.push('The username must be supplied.')
  }else if (username.length < 3 || username.length > 50) {
    errors.push('The username must be at least 3 characters and cannot be more than 50 characters.')
  }else if (username.indexOf(' ') !== -1) {
    errors.push('The username cannot contain empty spaces.')
  }

  if (typeof password == 'undefined') {
    errors.push('The password must be supplied.')
  } else if (password.length < 6) {
    errors.push('The password must contain at least 6 characters.')
  }else if (password.indexOf('#') == -1 && 
    password.indexOf('%') == -1 && 
    password.indexOf('!') == -1 && 
    password.indexOf('$') == -1 && 
    password.indexOf('*') == -1 ) {
    errors.push('The password should contain at least one of the following characters: #,%,!,$,* ')
  }
  
  if (errors.length > 0) {
    return res.status(400).json({ errors })
  } else {
    next()
  }

}

exports.securePassword = (req, res, next) => {
  const utils = require('./utils')
  req.body.password = utils.hash(req.body.password)
  next()
}

exports.checkAuth = (req, res, next) => {
  const jwt = require('jsonwebtoken')
  const token = req.headers['x-access-token']
  if (token) {
    jwt.verify(token, process.env.SECRET, function(err, decoded) {
      if (err) {
        if (err.name == 'TokenExpiredError') {
          return res.status(401).json({ errors: ['Token expired'] }) 
        }
        return res.status(500).json({ errors: ['Failed to authenticate token, '+ err.message || ''] })
      }
      next()
    });
  } else {
    return res.status(401).json({ errors: ['Unauthorized'] })
  } 

}