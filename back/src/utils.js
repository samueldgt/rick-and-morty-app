const crypto = require('crypto')
const salt = 'bbf13ae4db87d475ca0ee5f97e397248'

exports.hash = (value) => {
  return crypto.pbkdf2Sync(value, salt, 1000, 64, 'sha512').toString('hex')
}
