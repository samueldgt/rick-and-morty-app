const express = require('express')
const controller = require('./controller')
const router = express.Router()
const { 
  checkAuth,
  validatePageParam, 
  validateUsernameAndPassword, 
  securePassword } = require('./middlewares')

router.get('/characters', [checkAuth, validatePageParam], controller.index)
router.post('/sign-up', [ validateUsernameAndPassword, securePassword ], controller.signUp)
router.post('/login', securePassword , controller.login)
router.delete('/user', controller.remove)

module.exports = router