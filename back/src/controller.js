const axios = require('axios').default
let db = require('./database')
const jwt = require('jsonwebtoken')

exports.index = async (req, res) => {
  try {
    const page = req.query.page
    const call = await axios.get(`https://rickandmortyapi.com/api/character/?page=${page}`)
    const urlWithoutPageNumber = 'https://rickandmortyapi.com/api/character/?page=' 
    result = {}
    result.info = call.data.info || {}

    if (typeof result.info.prev !== 'undefined' && result.info.prev.length > 0) {
      let prevPage = result.info.prev.replace(urlWithoutPageNumber, '')
      result.info.prev = parseInt(prevPage)
    } else {
      result.info.prev = ''
    }

    if (typeof result.info.next !== 'undefined' && result.info.next.length > 0) {
      let nextPage = result.info.next.replace(urlWithoutPageNumber, '')
      result.info.next = parseInt(nextPage)
    } else {
      result.info.next = ''
    }
    
    result.data = (typeof call.data.results !== 'undefined' && call.data.results && call.data.results.length > 0) ?call.data.results.map(character => {
      return {
        name: character.name,
        status: character.status,
        species: character.species,
        gender: character.gender,
        image: character.image
      }
    }) : []
    res.status(200).json(result) 
  } catch (err) {
    console.error(err)
    res.status(400).json({ errors: [err] })
  }
}

exports.signUp = (req, res) => {
  const username = req.body.username
  const password = req.body.password
  db.get(username, function (err, reply) {
    if (err) {
      res.status(500).json({ errors: [err] })
    } else if (reply) {
      res.status(400).json({ errors: ['The username already exist.'] })
    } else {
      db.set(username, password)
      res.status(200).json({ message: 'Success' })
    }
  })
}

exports.login = (req, res) => {
  const username = req.body.username
  const password = req.body.password
  let token = ''
  db.get(username, function (err, reply) {
    if (err) {
      res.status(500).json({ errors: [err] })
    } else if (reply) {
      if (reply == password) {
        token = jwt.sign(
          { 'username': username }, // payload 
          process.env.SECRET, // secret key
          { expiresIn: "2m" } // expires in 2 min
        )  
        res.status(200).json({ token })
      } else {
        res.status(400).json({ errors: ['The password is incorrect.'] })
      }
    } else {
      res.status(400).json({ errors: ['The username provided does not exist.'] })
    }
  })
}

exports.remove = (req, res) => {
  const username = req.body.username
  db.del(username, function (err, reply) {
    if (err){
      res.status(500).json({ errors: [err] })
    } else if (reply === 1) {
      res.status(200).json({ message: 'Success' })
    } else {
      res.status(400).json({ errors: ['The user does not exist.'] })
    } 
  })
}
