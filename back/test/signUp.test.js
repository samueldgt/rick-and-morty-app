let chai = require('chai')
let chaiHttp = require('chai-http')
let db = require('../src/database')
let should = chai.should()
let server = require('../app')

chai.use(chaiHttp)

describe('Sign Up Tests', () => {
  before((done) => {   //Before each test we empty the database
    db.flushdb( function (err, succeeded) {
      if (succeeded) done()
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user without username', (done) => {
      let body = {
        password: "12345",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The username must be supplied.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user without password', (done) => {
      let body = {
        username: "TheLord",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The password must be supplied.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user if username conains empty spaces', (done) => {
      let body = {
        username: "The Lord",
        password: "12345",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The username cannot contain empty spaces.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user if username has less than 3 characters', (done) => {
      let body = {
        username: "sa",
        password: "12345",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The username must be at least 3 characters and cannot be more than 50 characters.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user if username has more than 50 characters', (done) => {
      let body = {
        username: "UM88bWKKJsEC5cKaXiV4jOitK0Mmx50FOQoQXWrRSqF0sEf6iQb",
        password: "12345",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The username must be at least 3 characters and cannot be more than 50 characters.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user if password has less than 6 characters', (done) => {
      let body = {
        username: "John Titor",
        password: "123",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The password must contain at least 6 characters.')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should not create a user if password not contains at least one of the following characters: #,%,!,$,*', (done) => {
      let body = {
        username: "JohnTitor",
        password: "123456",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('errors')
        res.body.errors.should.be.a('array')
        res.body.errors.length.should.not.be.eql(0)
        res.body.errors.should.include('The password should contain at least one of the following characters: #,%,!,$,* ')
        done()
      })
    })
  })

  describe('/POST sign-up', () => {
    it('it should create a user', (done) => {
      let body = {
        username: "JohnTitor",
        password: "12345!",
      }
      chai.request(server)
      .post('/sign-up')
      .send(body)
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('message')
        res.body.message.should.be.a('string')
        res.body.message.should.be.eql('Success')
        db.get(body.username, function (err, reply) {
          if (reply) {
            done()
          }
        })
      })
    })
  })
})
