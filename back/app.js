require('dotenv').config()
const express = require('express')
const helmet = require('helmet') // Increases API security by headers
const cors = require('cors') // Adds CORS (accepts requests coming from other origins)
const routes = require('./src/router')
const port = process.env.BACKEND_PORT

const app = express()

// middleware configuration 
app.use(helmet())
app.use(cors())
app.use(express.json())

app.use('/', routes)

app.listen(port, () => {
  console.info(`Server running on port ${ port }`)
})

module.exports = app // for tests