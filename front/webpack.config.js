const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './src/index',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    // publicPath: '/'
  },
  module: {
    rules: [
      { test: /\.js$/, include: path.resolve(__dirname, 'src'), use: ['babel-loader'] },
      { test : /\.css$/, use: ['style-loader', 'css-loader'] },
      { test : /\.(png|jpg|gif)$/, use: ['file-loader'] }
    ]
  },
  devServer: {
    contentBase:  path.resolve(__dirname, 'dist'),
    port: 4012,
    historyApiFallback: true,
    hot: true,
    open: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html" //source html
    })
  ]
}
