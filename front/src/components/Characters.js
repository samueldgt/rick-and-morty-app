import React from 'react'
import axios from 'axios'
import imgLoading from '../assets/images/loading.gif'
import config from '../config'

class Characters extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      token: '',
      info: {},
      characters:[],
      loading: true
    }
  }

  handleNavigation = (e) => {
    this.setState((prevState, props) => {
      return {
        ...prevState,
        loading: true 
      }
    })
    this.fetchData(e.target.getAttribute('page'))
  }

  getFormattedCharacters = () => {
    let row = ''
    return this.state.characters.map((char, index) => {
      row = ( (index + 1) % 4 == 0 ) ? (<div className="w-100" key={'r' + index}></div>) : ''
      return(
        <React.Fragment key={'frag-'+index}>
          <div className="col-3" key={index}>
            <div className="card" key={'card' + index}>
              <img src={char.image} className="card-img-top" alt={char.name} key={'img' + index} />
              <div className="card-body" key={'card-body' + index}>
                <h5 className="card-title" key={'card-title' + index}>{char.name}</h5>
                <div className="card-text" key={'card-text' + index}>
                  <p key={'l1' + index}><span className="font-weight-bold">Status: </span>{char.status}</p>
                  <p key={'l2' + index}><span className="font-weight-bold">Species: </span>{char.species}</p>
                  <p key={'l3' + index}><span className="font-weight-bold">Gender: </span>{char.gender}</p>
                </div>
              </div>
            </div>
          </div>
          {row}
        </React.Fragment>
      )
    })
  }

  getPaginationButtons = () => {
    let first = (typeof this.state.info.prev !== 'undefined' && this.state.info.prev !== '' ) ? 
      (
        <li className="page-item" key="first">
          <a className="page-link" href={'#page1'} onClick={this.handleNavigation} page="1" key="f0">First</a>
        </li>
      ) : ''
    let prev = (typeof this.state.info.prev !== 'undefined' && this.state.info.prev !== '' ) ? 
      (
        <li className="page-item" key="prev">
          <a className="page-link" href={'#page' + this.state.info.prev} onClick={this.handleNavigation} page={this.state.info.prev} key="f1">Previous</a>
        </li>
      ) : ''
    let next = (typeof this.state.info.next !== 'undefined' && this.state.info.next !== '' ) ? 
      (
        <li className="page-item" key="next">
          <a className="page-link" href={'#page' + this.state.info.next} onClick={this.handleNavigation} page={this.state.info.next} key="f2">Next</a>
        </li>
      ) : ''
    let last = (typeof this.state.info.next !== 'undefined' && this.state.info.next !== '' ) ? 
      (
        <li className="page-item" key="last">
          <a className="page-link" href={'#page' + this.state.info.pages} onClick={this.handleNavigation} page={this.state.info.pages} key="f3">Last</a>
        </li>
      ) : ''
    return (
      <nav aria-label="Page pagination">
        <ul className="pagination justify-content-center">
          {first}
          {prev}
          {next}
          {last}
        </ul>
      </nav>
    )
  }

  fetchData = (page = 1) => {
    axios.get(`${config.apiURL}/characters?page=${page}`,
      {
        headers: {'x-access-token': this.state.token }
      }
    )
    .then((result) => {
      this.setState({
        ...this.state,
        info: result.data.info, 
        characters: result.data.data,
        loading:false
      })
    })
    .catch((error) => {
      this.setState((prevState, props) => {
        return {
          ...prevState,
          loading: false 
        }
      })

      if (error.response.status == 401) {
        this.props.authTimeOut()
        this.props.history.push('/login')
      } else {
        this.props.setNotification('', error.response.data.errors || [error.message, "The app couldn't connect to the server"])
      }
      
    })
  }

  componentDidMount() {
    if (this.props.getToken() == '') {
      this.props.history.replace('/login')
    } else {
      this.setState({
        ...this.state,
        token: this.props.getToken()
      })
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.token !== this.state.token && prevState.token == '') {
      this.fetchData()
    }
  }
  
  render () {
    return (this.state.loading) ? 
    (
      <div className="start col-sm-12 col-md-6 col-lg-6 text-center mt-5">
        <img src={imgLoading} className="mt-5" alt="Loading..."/>
      </div>
    ) :
    (
      <div className="characters col">
        <h2 className="text-center">Characters</h2>
        <div className="row">
          {this.getFormattedCharacters()}
        </div>
        <div className="row justify-content-center">
          <div className="col-sm-12 col-md-4 col-lg-4">
            {this.getPaginationButtons()}
          </div>
        </div>
      </div>
    )
  
  }
}

export default Characters