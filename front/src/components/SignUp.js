import React from 'react'
import axios from 'axios'
import config from '../config'

class SignUp extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      confirm: '',
      usernameStyle: '',
      passwordStyle: '',
      confirmStyle: '',
      errors: [],
      sendForm: false
    }
  }

  handleChange = (e) => {
    const name = e.target.id
    const value = e.target.value
    this.setState({
      [name]: value,
      usernameStyle: '',
      passwordStyle: '',
      confirmStyle: '',
      errors: [],
      sendForm: false
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let usernameStyle = ''
    let passwordStyle = ''
    let confirmStyle = ''
    let errors = []

    if( this.state.username == '' ) {      
      usernameStyle = 'is-invalid'
      errors.push('The username cannot be blank.')
    } 
    if( this.state.password == '' ) {
      passwordStyle = 'is-invalid'
      errors.push('The password cannot be blank.')
    }
    if( this.state.password !== this.state.confirm ) {
      confirmStyle = 'is-invalid'
      errors.push('Both passwords should match.')
    }
    this.setState({
      ...this.state,
      usernameStyle,
      passwordStyle,
      confirmStyle,
      errors,
      sendForm: true
    })
  }

  checkPasswords = (e) => {
    if (this.state.password !== e.target.value) {
      this.setState({
        ...this.state,
        confirmStyle: 'is-invalid',
        errors: ['Both passwords should match.']
      })
    }
  }

  getFormattedErrors = (errors) => {
    return errors.map( (error, key) => {
      return (
        <div key={key} className="alert alert-danger" role="alert">
          { error }
        </div>
      )
    })
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.sendForm !== prevState.sendForm && this.state.sendForm == true) {
      if (this.state.errors.length == 0) {        
        this.fetchData()
      }
      this.setState({
        ...this.state,
        sendForm: false
      })
    }
  }

  fetchData = () => {
    axios.post(config.apiURL + '/sign-up',
      {
        username: this.state.username,
        password: this.state.password
      }, {}
    )
    .then((result) => {
      console.log(result.data)
      this.props.setNotification('Your user account was created successfully')
      this.props.history.push('/login')
    })
    .catch((error) => {
      this.setState({
        ...this.state,
        errors: error.response.data.errors || [error.message, "The app couldn't connect to the server"]
      })
    })
  }

  render () {
    return (
      <div className="sign-up col-sm-12 col-md-8 col-lg-6">
        <h2 className="text-center">Sign Up</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input id="username" type="text" className={`form-control ${this.state.usernameStyle}`} 
              onChange={this.handleChange} value={this.state.username} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input id="password" type="password" className={`form-control ${this.state.passwordStyle}`}
              onChange={this.handleChange} value={this.state.password} />
          </div>
          <div className="form-group">
            <label htmlFor="confirm">Confirm Password</label>
            <input id="confirm" type="password" className={`form-control ${this.state.confirmStyle}`}
              onChange={this.handleChange} value={this.state.confirm} onBlur={this.checkPasswords} />
          </div>
          { this.getFormattedErrors(this.state.errors) }
          <button type="submit" className="btn btn-info">Sign Up</button>
        </form>
      </div>
    )
  }
}

export default SignUp
