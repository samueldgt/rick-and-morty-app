import React from 'react'
import axios from 'axios'
import config from '../config'

class Login extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      usernameStyle: '',
      passwordStyle: '',
      errors: [],
      sendForm: false
    }
  }

  handleChange = (e) => {
    const name = e.target.id
    const value = e.target.value
    this.setState({
      [name]: value,
      usernameStyle: '',
      passwordStyle: '',
      errors: [],
      sendForm: false
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let usernameStyle = ''
    let passwordStyle = ''
    let errors = []

    if( this.state.username == '' ) {      
      usernameStyle = 'is-invalid'
      errors.push('The username cannot be blank.')
    } 
    if( this.state.password == '' ) {
      passwordStyle = 'is-invalid'
      errors.push('The password cannot be blank.')
    }

    this.setState({
      ...this.state,
      usernameStyle,
      passwordStyle,
      errors,
      sendForm: true
    })
  }

  getFormattedErrors = () => {
    return this.state.errors.map( (error, key) => {
      return (
        <div key={key} className="alert alert-danger" role="alert">
          { error }
        </div>
      )
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.sendForm !== prevState.sendForm && this.state.sendForm == true) {
      if (this.state.errors.length == 0) {        
        this.fetchData()
      }
      this.setState({
        ...this.state,
        sendForm: false
      })
    }
  }

  fetchData = () => {
    axios.post( config.apiURL + '/login',
      {
        username: this.state.username,
        password: this.state.password
      }, {}
    )
    .then((result) => {
      this.props.auth(result.data.token, this.state.username)
      this.props.history.push('/')

    })
    .catch((error) => {
      
      this.setState({
        ...this.state,
        errors: error.response.data.errors || [error.message, "The app couldn't connect to the server"]
      })
    })
  }

  render () {
    return (
      <div className="login col-sm-12 col-md-8 col-lg-6">
        <h2 className="text-center">Login</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input id="username" type="text" className={`form-control ${this.state.usernameStyle}`} 
              onChange={this.handleChange} value={this.state.username} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input id="password" type="password" className={`form-control ${this.state.passwordStyle}`}
              onChange={this.handleChange} value={this.state.password} />
          </div>
          { this.getFormattedErrors() }
          <button type="submit" className="btn btn-info">Login</button>
        </form>
      </div>
    )
  }
}

export default Login