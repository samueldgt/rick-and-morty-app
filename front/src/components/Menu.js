import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import Logo from '../assets/images/logo.png'

class Menu extends React.Component {
  constructor(props) {
    super(props)
  }

  getAuthNavLinks = () => {
    let links = ''
    if (this.props.user.token == '') {
      links = (
        <>
        <li className="nav-item">
          <NavLink className="nav-link" to="/login">Login</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/sign-up">Sign Up</NavLink>
        </li>
        </>
      )
    } else {
      links = (
        <li className="nav-item">
          <NavLink className="nav-link" to="/">Characters</NavLink>
        </li>
      )
    }
    return links
  }

  render(){
    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <div className="container">
          
          <Link to="/" className="navbar-brand"><img src={Logo} height="58" alt="Rick And Morty"/></Link>

          <ul className="navbar-nav">
            {this.getAuthNavLinks()}
            <li className="nav-item">
              <NavLink className="nav-link" to="/about">About</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default Menu