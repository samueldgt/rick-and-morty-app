import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Menu from './Menu'
import About from './About'
import NotFound from './NotFound'
import SignUp from './SignUp'
import Login from './Login'
import Characters from './Characters'
import styles from '../assets/css/styles.css'

class App extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      token: '',
      username: '',
      message: '',
      errors: []
    }
  }

  getToken = () => {
    return this.state.token
  }

  setNotification = (message = '', errors = []) => {
    this.setState({
      ...this.state,
      message,
      errors
    })
  }
  
  getFormattedNotifications = () => {
    if (this.state.message !== '') {
      return (
        <div className="col-12 text-center alert alert-success" role="alert">
          { this.state.message }
        </div>
      )
    }
    if (this.state.errors.length > 0) {
      return this.state.errors.map( (error, key) => {
        return (
          <div key={key} className="col-12 text-center alert alert-danger" role="alert">
            { error }
          </div>
        )
      })
    }
  }

  auth = (token, username) => {
    this.setState({
      ...this.state,
      token,
      username
    })
  }

  authTimeOut = () => {
    this.setState((prevState, props) => {
      return {
        token: '',
        username: '',
        message: '',
        errors: ['The session has expired, please login again to continue.']
      }
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if ((prevState.message == '' && prevState.message !== this.state.message)
      || (prevState.errors.length === 0 && this.state.errors.length > 0)) {
      setTimeout(() => this.setState({
        ...this.state, 
        message: '', 
        errors: []
      }), 3000);
    }
  }

  render() {
    
    return (
      <Router>
        <div className="app">
          <Menu user={this.state} />
          <div className="container content">
            <div className="row justify-content-center">
              {this.getFormattedNotifications()}
              <Switch>
                <Route exact path="/" render={ (props) => <Characters {...props} getToken={this.getToken} setNotification={this.setNotification} authTimeOut={this.authTimeOut} /> } />
                <Route path="/login" render={ (props) => <Login {...props} auth={this.auth} /> } />
                <Route path="/sign-up" render={ (props) => <SignUp {...props}  setNotification={this.setNotification} /> } />
                <Route path="/about" component={About} />
                <Route path="*" component={NotFound} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    )
  }
}

export default App