**Rick and Morty App by Samuel Gonzalez**

A fast start having Docker installed:

CLone this repo

* Open the project folder in a terminal and cd inside
* Run the command "docker-compose up --build"
* Open the URL in a browser http://localhost:4012
* Enjoy it...
